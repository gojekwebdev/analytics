# Analytics
Short description and motivation.

## Usage
How to use my plugin.
here references what i use

[google-client](https://github.com/google/google-api-ruby-client/tree/master/lib/google/apis)
[CoryFoy](https://gist.github.com/CoryFoy/9edf1e039e174c00c209e930a1720ce0)
[google-auth](https://github.com/google/google-auth-library-ruby)


## Installation
Add this line to your application's Gemfile:

```ruby
gem 'analytics'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install analytics
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
