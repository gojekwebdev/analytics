Analytics::Engine.routes.draw do
	namespace 'api' do
		# resource :analytics, only: [:index, :show]
		get '/analytics/:id', to: 'analytics#show'
		get '/analytics/:id/summary', to: 'analytics#summary'
	end

	get '/analytics', to: 'dashboards#index'

end
