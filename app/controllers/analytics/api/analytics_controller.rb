module Analytics
	module Api
	  class AnalyticsController < ApplicationController

	  	# API /api/analytics/:id/summary
	  	# @params id(params :id), start_date, end_date
	  	# 
	  	def summary
	  		ga = Analytics::Gasite.new
	  		opts = {
	        	"start_date" 	=> params[:start_date].present? ? params[:start_date] : "7daysAgo",
	        	"end_date"		=> params[:end_date].present? ? params[:end_date] : "today"
	        }
	        @res = ga.summary(params[:id], opts)
	  		render json: ga.chart_sum_json
	  	end

	  	# API /api/analytics/:id
	  	# @params id(params :id), dim(params :dim [day, month, year, user, browser, country, city n etc]), start_date, end_date
	  	# 
	    def show
	        ga = Analytics::Gasite.new
	        opts = {
	        	"start_date" 	=> params[:start_date].present? ? params[:start_date] : "7daysAgo",
	        	"end_date"		=> params[:end_date].present? ? params[:end_date] : "today"
	        }
	    	@res = ga.dimensions(params[:id],params[:dim], opts)
	    	render json: ga.chart_dim_json
	    end

	  end
	end
end
